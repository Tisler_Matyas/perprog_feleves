﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Diagnostics;

namespace kep_kodolas
{
    class Program
    {
        static Random rnd;
        static void Main(string[] args)
        {
            Stopwatch sw = new Stopwatch();
            sw.Start();
            rnd = new Random();
            Bitmap kepem = new Bitmap("small.jpg");
            int[] adatok = new int[(kepem.Height * kepem.Width)/2];
            List<int> valaszthato_pixelek = new List<int>();
            for (int i = 0; i < adatok.Length; i++)
            {
                valaszthato_pixelek.Add(i + (kepem.Height * kepem.Width / 2));
            }
            ;
            for (int i = 0; i < adatok.Length; i++)
            {
                int random_pixel = rnd.Next(valaszthato_pixelek.Count); //kiválasztok random egy pixelt
                int kivalasztott_pixel = valaszthato_pixelek[random_pixel];
                int y = kivalasztott_pixel / kepem.Width;
                int x = kivalasztott_pixel - (kepem.Width * y);
                valaszthato_pixelek.Remove(kivalasztott_pixel);

                adatok[i] = kivalasztott_pixel;
                
                    
                int adatok_y = i / kepem.Width;
                int adatok_x = i - (kepem.Width * adatok_y);
                Color temp_color = kepem.GetPixel(x, y);
                kepem.SetPixel(x, y, kepem.GetPixel(adatok_x, adatok_y));
                kepem.SetPixel(adatok_x, adatok_y, temp_color);
            }

            kepem.Save("rejtett_kep.png");


            //visszafejtés

            for (int i = 0; i < adatok.Length; i++)
            {
                int kivalaszott_pixel = adatok[i];
                int y = kivalaszott_pixel / kepem.Width;
                int x = kivalaszott_pixel - (kepem.Width * y);

                Color temp_color = kepem.GetPixel(x, y);

                int adatok_y = i / kepem.Width;
                int adatok_x = i - (kepem.Width * adatok_y);

                kepem.SetPixel(x, y, kepem.GetPixel(adatok_x, adatok_y));
                kepem.SetPixel(adatok_x, adatok_y, temp_color);

            }

            kepem.Save("kikodolt_kep.png");

            sw.Stop();
            Console.WriteLine(sw.Elapsed.TotalSeconds);
            Console.WriteLine("Kész!");
            Console.ReadKey();
            
        }
    }
}
